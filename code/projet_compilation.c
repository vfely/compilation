/*
	Tests :
		./a.out "main() { int x x = 2 if x <= 3 x = 2 else y = 2 } "
*/

#include <stdio.h>
#include <stdlib.h>
#include "projet_compilation.h"
#include <string.h>

void init_AEF(int taille, int AEF[TAILLE_MAX][TAILLE_MAX]) {
	int i,j;
	for (i=0; i<taille; i++) {
		for (j=0; j<taille; j++) {
			AEF[i][j] = -1;
		}
	}
}

void affiche_AEF(int taille, int AEF[TAILLE_MAX][TAILLE_MAX]) {
	int i,j;	
	for (i=0; i<taille; i++) {
		for (j=0; j<taille; j++) {
			printf("%i ",AEF[i][j]);
		}
	printf("\n");
	}
}

int est_motcle (char chaine[TAILLE_CHAINE]) {

	int AEF[TAILLE_MAX][TAILLE_MAX];
	init_AEF(TAILLE_LETTRES,AEF);

	AEF[0]['m'-'a'] = 1;	// m
	AEF[1]['a'-'a'] = 2;	// a
	AEF[2]['i'-'a'] = 3;	// i
	AEF[3]['n'-'a'] = 4;	// n
	AEF[4][27] = 5;			// (
	AEF[5][28] = 6;			// )
	AEF[6][29] = 26;		// Espace
	
	AEF[0]['i'-'a'] = 7;	// i
	AEF[7]['f'-'a'] = 8;	// f
	AEF[8][29] = 26;		// Espace
	
							// i
	AEF[7]['n'-'a'] = 9;	// n
	AEF[9]['t'-'a'] = 10;	// t
	AEF[10][29] = 26;		// Espace
	
	AEF[0]['f'-'a'] = 11;	// f
	AEF[11]['l'-'a'] = 12;	// l
	AEF[12]['o'-'a'] = 13;	// o
	AEF[13]['a'-'a'] = 14;	// a
	AEF[14]['t'-'a'] = 15;	// t
	AEF[15][29] = 26;		// Espace

	AEF[0]['c'-'a'] = 16;	// c
	AEF[16]['i'-'a'] = 17;	// i
	AEF[17]['n'-'a'] = 18;	// n
	AEF[18][29] = 26;		// Espace
	
							// c
	AEF[16]['o'-'a'] = 19;	// o
	AEF[19]['u'-'a'] = 20;	// u
	AEF[20]['t'-'a'] = 21;	// t
	AEF[21][29] = 26;		// Espace
	
	AEF[0]['e'-'a'] = 22;	// e
	AEF[22]['l'-'a'] = 23;	// l
	AEF[23]['s'-'a'] = 24;	// s
	AEF[24]['e'-'a'] = 25;	// e
	AEF[25][29] = 26;		// Espace
	
	int etat_suivant = 0;
	int c;
	int i=0;
	while (chaine[i] != '\0') {
		if (chaine[i] >= 'a' && chaine[i] <= 'z')
			c = chaine[i]-'a';
		
		else if (chaine[i] == '(' || chaine[i] == ')')
			c = chaine[i]-'('+'z'-'a'+2;

		else if (chaine[i] == ' ')
			c = 29;
		
		else return 0;

		/* DEBUG
		printf("Caractère %c \n", chaine[i]);
		printf("Numero de caractère = %i \n", c);
		printf("AEF[%i][%i] (mots clés) = %i\n\n", etat_suivant, c, AEF[etat_suivant][c]);
		*/

		if (AEF[etat_suivant][c] != -1) {
			etat_suivant = AEF[etat_suivant][c];
			//printf("Etat suivant %i\n\n", etat_suivant); // DEBUG
		}
		
		else return 0;
		
		i++;
	}

	return 1;
}

int est_lettre (char c) {
	if (isalpha (c))
		return 1;
	return 0;
}

int est_chiffre (char c) {
	if (isdigit (c))
		return 1;
	return 0;
}

int est_identificateur(char chaine[TAILLE_CHAINE]) {
	int AEF[TAILLE_MAX][TAILLE_MAX];
	init_AEF(TAILLE_LETTRES,AEF);

	
	AEF[0][0] = 1;	// 1er caractère = lettre 
	AEF[0][2] = 1;	// ou underscore -> Passage à état 1

	AEF[1][0] = 1;	// Autres caractères = lettre
	AEF[1][1] = 1;	// ou chiffre
	AEF[1][2] = 1;	// ou underscore -> Boucle sur état 1
	AEF[1][3] = 2;	// ou espace -> Passage à état 2 (fin)

	int etat_suivant = 0;
	int c;
	int i = 0;
	while (chaine[i] != '\0') {
		if (est_lettre(chaine[i]))
			c = 0;
		
		else if (est_chiffre(chaine[i]))
			c = 1;

		else if (chaine[i] == '_')
			c = 2;

		else if (chaine[i] == ' ')
			c = 3;
		
		else return 0;

		/* DEBUG
		printf("Caractère %c \n", chaine[i]);
		printf("Numero de caractère = %i \n", c);
		printf("AEF[%i][%i] (identificateur) = %i\n\n", etat_suivant, c, AEF[etat_suivant][c]);
		*/

		if (AEF[etat_suivant][c] != -1) {
			etat_suivant = AEF[etat_suivant][c];
			//printf("Etat suivant %i\n\n", etat_suivant); // DEBUG
		}
		
		else return 0;
		
		i++;
	}
	
	return 1;
}

int est_constante(char chaine[TAILLE_CHAINE]) {
	int AEF[TAILLE_MAX][TAILLE_MAX];
	init_AEF(3,AEF);

	AEF[0][0] = 1; // Premier caractère = chiffre -> Passage à l'état 1
	AEF[1][0] = 1; // Autres caractères chiffres
	AEF[1][1] = 2; // ou espace -> Passage à état 2 (fin)

	int etat_suivant = 0;
	int c;
	int i=0;
	while (chaine[i] != '\0') {
		if (est_chiffre(chaine[i]))
			c = 0;

		else if (chaine[i] == ' ')
			c = 1;

		else return 0;

		/* DEBUG
		printf("Caractère %c \n", chaine[i]);
		printf("Numero de caractère = %i \n", c);
		printf("AEF[%i][%i] (constante) = %i\n\n", etat_suivant, c, AEF[etat_suivant][c]);
		*/

		if (AEF[etat_suivant][c] != -1) {
			etat_suivant = AEF[etat_suivant][c];
			//printf("Etat suivant %i\n\n", etat_suivant); // DEBUG
		}
		
		else return 0;

		i++;
	}

	return 1;
}

int est_operateur (char chaine[TAILLE_MAX]) {
	int AEF[TAILLE_MAX][TAILLE_MAX];
	init_AEF(TAILLE_LETTRES,AEF);

	AEF[0][0] = 1;	// Premier caractère = '<'
	AEF[0][1] = 1;	// ou '>'
	AEF[0][2] = 2;	// ou '='
	AEF[0][3] = 1;	// ou '!'

	// Si premier caractère = '!'
	AEF[1][2] = 2;	// second caractère = '=' -> Passage à l'état 2
	AEF[1][4] = 3;	// ou espace -> Fin

	AEF[2][4] = 3; // Si espace -> Fin

	int etat_suivant = 0;
	int c;
	int i=0;
	while (chaine[i] != '\0') {
		if (chaine[i] == '<')
			c = 0;

		else if (chaine[i] == '>')
			c = 1;

		else if (chaine[i] == '=')
			c = 2;
		
		else if (chaine[i] == '!')
			c = 3;

		else if (chaine[i] == ' ')
			c = 4;

		else return 0;

		/* DEBUG
		printf("Caractère %c \n", chaine[i]);
		printf("Numero de caractère = %i \n", c);
		printf("AEF[%i][%i] (constante) = %i\n\n", etat_suivant, c, AEF[etat_suivant][c]);
		*/
		
		if (AEF[etat_suivant][c] != -1) {
			etat_suivant = AEF[etat_suivant][c];
			//printf("Etat suivant %i\n\n", etat_suivant); // DEBUG
		}
		
		else return 0;

		i++;
	}

	return 1;
}

int est_accolade (char chaine[TAILLE_CHAINE]) {
	int AEF[TAILLE_MAX][TAILLE_MAX];
	init_AEF(TAILLE_LETTRES,AEF);

	AEF[0][0] = 1;	// Premier caractère = '{' -> Passage à l'état 1
	AEF[0][1] = 1; // ou = '}' -> Passage à l'état 1

	AEF[1][2] = 2; // Si ' ' -> Fin

	int etat_suivant = 0;
	int c;
	int i=0;
	while (chaine[i] != '\0') {
		if (chaine[i] == '{')
			c = 0;

		else if (chaine[i] == '}')
			c = 1;

		else if (chaine[i] == ' ')
			c = 2;

		else return 0;

		/* DEBUG
		printf("Caractère %c \n", chaine[i]);
		printf("Numero de caractère = %i \n", c);
		printf("AEF[%i][%i] (accolade) = %i\n\n", etat_suivant, c, AEF[etat_suivant][c]);
		*/
		
		if (AEF[etat_suivant][c] != -1) {
			etat_suivant = AEF[etat_suivant][c];
			//printf("Etat suivant %i\n\n", etat_suivant); // DEBUG
		}
		
		else return 0;

		i++;
	}

	return 1;
}
	
int analyseur_lexical (char chaine[TAILLE_CHAINE]) {
	if (est_motcle(chaine)) {
		printf("Mot clé reconnu\n");
		return 1;
	}

	if (est_identificateur(chaine)) {
		printf("Identificateur reconnu\n");
		return 1;
	}

	if (est_constante(chaine)) {
		printf("Constante reconnue\n");
		return 1;
	}

	if (est_operateur(chaine)) {
		printf("Operateur reconnu\n");
		return 1;
	}

	if (est_accolade(chaine)) {
		printf("Accolade reconnue\n");
		return 1;
	}

	printf("Mot non-reconnu\n");
	return 0;
}

int est_vide (Pile *p_pile) {
	if (p_pile->donnee == NULL)
		return 1;
	return 0;
}

Pile *init() {
	//printf("init()\n");
	
	Pile *pile = NULL;
	
	return pile;
}

void affiche_pile(Pile *p_pile) {
	Pile *p = p_pile;
	if (p == NULL) {
		printf("La pile est vide\n");
	}
	
	while(p != NULL) {
		printf("=> %s \n", p->donnee);
		p = p->precedent;
	}
	printf("\n");
}

Pile *empiler(Pile *p_pile, char *sommet)
{
	char *tmp;
	tmp = malloc(sizeof(char[TAILLE_MOT]));
	strcpy(tmp,sommet);
	
	Pile *p = malloc(sizeof(Pile));
	p->donnee = tmp;
	p->precedent = p_pile;
	p_pile = p;
	
	return p_pile;
}

Pile *depiler(Pile *p_pile)
{
	if (p_pile != NULL)
    {
    	Pile *tmp = p_pile->precedent;
		free(p_pile->donnee);
		free(p_pile);
		
		return tmp;
    }
    return NULL;
}

Pile *decoupage_chaine (char chaine[TAILLE_CHAINE]) {
	char mot[TAILLE_MOT];
	int p_chaine = 0;
	int p_mot = 0;
	Pile *p_pile = init();
	
	while(chaine[p_chaine] != '\0') {
		// Création du mot
		while(chaine[p_chaine] != ' ' && chaine[p_chaine] != '\n' && chaine[p_chaine] != '\0') {
			mot[p_mot] = chaine[p_chaine];
			p_mot++;
			p_chaine++;
		}
		// Fin du mot
		mot[p_mot] = ' ';
		mot[p_mot+1] = '\0';
		p_mot = 0;

		// Analyse du mot
		printf(" %s : ", mot);
		analyseur_lexical(mot);
		p_pile = empiler(p_pile, mot);
		
		// Si la chaine n'est pas finie on continue de la lire
		if (chaine[p_chaine] != '\0')
			p_chaine++;
	}
	
	// Ajout du $
	p_pile = empiler(p_pile,"$");
	
	// Retournement de la pile 
	Pile *p_tmp = init(); 
	while(p_pile != NULL) {
		p_tmp = empiler(p_tmp,p_pile->donnee);
		p_pile = p_pile->precedent;
	}
	
	return p_tmp;
}

int table_analyse (int x, int y) {
	int TA[100][100];
	
	// Init à -1
	int i,j;
	for (i=0; i<100; i++) {
		for (j=0; j<100; j++) {
			TA[i][j] = -1;
		}
	}
	
	// <programme>
	TA[1][1] = 1; // main()
	
	// <liste_declarations>
	TA[2][2] = 22; // }
	TA[2][4] = 21; // int
	TA[2][5] = 21; // float
	TA[2][3] = 22; // id
	TA[2][6] = 22; // cin
	TA[2][7] = 22; // cout
	TA[2][11] = 22; // if
	TA[2][14] = 22; // $
	
	// <declaration>
	TA[3][4] = 3; // int
	TA[3][5] = 3; // float
	
	// <type>
	TA[4][4] = 41; // int
	TA[4][5] = 42; // float
	
	// <liste_instructions>
	TA[5][2] = 52; // }
	TA[5][3] = 51; // id
	TA[5][6] = 51; // cin
	TA[5][7] = 51; // cout
	TA[5][11] = 51; // if
	TA[5][14] = 52; // $
	
	// <instruction>
	TA[6][3] = 62; // id
	TA[6][6] = 61; // cin
	TA[6][7] = 61; // cout
	TA[6][11] = 63; // if
	
	// <E/S>
	TA[7][6] = 71; // cin
	TA[7][7] = 72; // cout
	
	// <saisie>
	TA[8][6] = 8; // cin
	
	// <affichage>
	TA[9][7] = 9; // cout
	
	// <affectation>
	TA[10][3] = 10; // id
	
	// <test>
	TA[11][11] = 11; // if
	
	// <condition>
	TA[12][3] = 12; // id
	
	// <operateur>
	TA[13][18] = 131; // <
	TA[13][19] = 132; // >
	TA[13][20] = 133; // <=
	TA[13][21] = 134; // >=
	TA[13][22] = 135; // ==
	TA[13][23] = 136; // !=
	
	return TA[x][y];
}

int num_tab_nterminal (char *sommet) {
	
	if (strcmp("<programme>", sommet) == 0)
		return 1;
	else if (strcmp("<liste_declarations>", sommet) == 0)
		return 2;
	else if (strcmp("<declaration>", sommet) == 0)
		return 3;
	else if (strcmp("<type>", sommet) == 0)
		return 4;
	else if (strcmp("<liste_instructions>", sommet) == 0)
		return 5;
	else if (strcmp("<instruction>", sommet) == 0)
		return 6;
	else if (strcmp("<E/S>", sommet) == 0)
		return 7;
	else if (strcmp("<saisie>", sommet) == 0)
		return 8;
	else if (strcmp("<affichage>", sommet) == 0)
		return 9;
	else if (strcmp("<affectation>", sommet) == 0)
		return 10;
	else if (strcmp("<test>", sommet) == 0)
		return 11;
	else if (strcmp("<condition>", sommet) == 0)
		return 12;
	else if (strcmp("<operateur>", sommet) == 0)
		return 13;
	else return -1;
}

int num_tab_terminal (char *sommet) {
	
	if (strcmp("main() ", sommet) == 0)
		return 1;
	else if (strcmp("} ", sommet) == 0)
		return 2;
	else if (strcmp("id", sommet) == 0)
		return 3;
	else if (strcmp("int ", sommet) == 0)
		return 4;
	else if (strcmp("float ", sommet) == 0)
		return 5;
	else if (strcmp("cin ", sommet) == 0)
		return 6;
	else if (strcmp("cout ", sommet) == 0)
		return 7;
	else if (strcmp("<< ", sommet) == 0)
		return 8;
	else if (strcmp(">> ", sommet) == 0)
		return 9;
	else if (strcmp("if ", sommet) == 0)
		return 11;
	else if (strcmp("else ", sommet) == 0)
		return 12;
	else if (strcmp("operateur", sommet) == 0)
		return 13;
	else if (strcmp("$", sommet) == 0)
		return 14;
	else if (strcmp("{ ", sommet) == 0)
		return 15;
	else if (strcmp("nombre", sommet) == 0)
		return 16;
	else if (strcmp("= ", sommet) == 0)
		return 17;
	else if (strcmp("< ", sommet) == 0)
		return 18;
	else if (strcmp("> ", sommet) == 0)
		return 19;
	else if (strcmp("<= ", sommet) == 0)
		return 20;
	else if (strcmp(">= ", sommet) == 0)
		return 21;
	else if (strcmp("== ", sommet) == 0)
		return 22;
	else if (strcmp("!= ", sommet) == 0)
		return 23;
	
	else return -1;
}

Pile *empiler_programme (int num_tab, Pile *p_programme) {
	Pile *p_tmp = init();
	
	switch(num_tab) {
		case 1 : // <programme>
			printf("<programme> ::= main() { <liste_declarations> <liste_instructions> }\n");
			p_tmp = empiler(p_tmp, "main() ");
			p_tmp = empiler(p_tmp, "{ ");
			p_tmp = empiler(p_tmp, "<liste_declarations>");
			p_tmp = empiler(p_tmp, "<liste_instructions>");
			p_tmp = empiler(p_tmp, "} ");
			p_tmp = empiler(p_tmp, "$");
		break;
		
		case 21 : // <liste_declarations>
			printf("<liste_declarations> ::= <declaration> <liste_declarations>\n");
			p_tmp = empiler(p_tmp, "<declaration>");
			p_tmp = empiler(p_tmp, "<liste_declarations>");
		break;
		
		case 22 : // <liste_declarations>
			printf("<liste_declarations> ::= vide\n");
		break;
		
		case 3 : // <declaration>
			printf("<declaration> ::= <type> id\n");
			p_tmp = empiler(p_tmp, "<type>");
			p_tmp = empiler(p_tmp, "id");
		break;
		
		case 41 : // <type>
			printf("<type> ::= int\n");
			p_tmp = empiler(p_tmp, "int ");
		break;
		
		case 42 : // <type>
			printf("<type> ::= float\n");
			p_tmp = empiler(p_tmp, "float ");
		break;
		
		case 51 : // <liste_instructions>
			printf("<liste_instructions> ::= <instructions> <liste_instructions>\n");
			p_tmp = empiler(p_tmp, "<instruction>");
			p_tmp = empiler(p_tmp, "<liste_instructions>");
		break;
		
		case 52 : // <liste_instructions>
			printf("<liste_instructions> ::= vide\n");
		break;
		
		case 61 : // <instruction>
			printf("<instruction> ::= <E/S>\n");
			p_tmp = empiler(p_tmp, "<E/S>");
		break;
		
		case 62 : // <instruction>
			printf("<instruction> ::= <affectation>\n");
			p_tmp = empiler(p_tmp, "<affectation>");
		break;
		
		case 63 : // <instruction>
			printf("<instruction> ::= <test>\n");
			p_tmp = empiler(p_tmp, "<test>");
		break;
		
		case 71 : // <E/S>
			printf("<E/S> ::= <saisie>\n");
			p_tmp = empiler(p_tmp, "<saisie>");
		break;
		
		case 72 : // <E/S>
			printf("<E/S> ::= <affichage>\n");
			p_tmp = empiler(p_tmp, "<affichage>");
		break;
		
		case 8 : // <saisie>
			printf("<saisie> ::= cin >> id\n");
			p_tmp = empiler(p_tmp, "cin ");
			p_tmp = empiler(p_tmp, ">> ");
			p_tmp = empiler(p_tmp, "id");
		break;
		
		case 9 : // <affichage>
			printf("<affichage> ::= cout << id\n");
			p_tmp = empiler(p_tmp, "cout ");
			p_tmp = empiler(p_tmp, "<< ");
			p_tmp = empiler(p_tmp, "id");
		break;
		
		case 10 : // <affectation>
			printf("<affectation> ::= id = nombre\n");
			p_tmp = empiler(p_tmp, "id");
			p_tmp = empiler(p_tmp, "= ");
			p_tmp = empiler(p_tmp, "nombre");
		break;
		
		case 11 : // <test>
			printf("<test> ::= if <condition> <instruction> else <instruction>\n");
			p_tmp = empiler(p_tmp, "if ");
			p_tmp = empiler(p_tmp, "<condition>");
			p_tmp = empiler(p_tmp, "<instruction>");
			p_tmp = empiler(p_tmp, "else ");
			p_tmp = empiler(p_tmp, "<instruction>");
		break;
		
		case 12 : // <condition>
			printf("<condition> ::= id <operateur> nombre\n");
			p_tmp = empiler(p_tmp, "id");
			p_tmp = empiler(p_tmp, "<operateur>");
			p_tmp = empiler(p_tmp, "nombre");
		break;
		
		case 131 : // <operateur>
			printf("<operateur> ::= <\n");
			p_tmp = empiler(p_tmp, "< ");
		break;
		
		case 132 : // <operateur>
			printf("<operateur> ::= >\n");
			p_tmp = empiler(p_tmp, "> ");
		break;
		
		case 133 : // <operateur> 
			printf("<operateur> ::= <=\n");
			p_tmp = empiler(p_tmp, "<= ");
		break;
		
		case 134 : // <operateur> 
			printf("<operateur> ::= >=\n");
			p_tmp = empiler(p_tmp, ">= ");
		break;
		
		case 135 : // <operateur> 
			printf("<operateur> ::= ==\n");
			p_tmp = empiler(p_tmp, "== ");
		break;
		
		case 136 : // <operateur> 
			printf("<operateur> ::= !=\n");
			p_tmp = empiler(p_tmp, "!= ");
		break;
	}
	
	while(p_tmp != NULL) {
		p_programme = empiler(p_programme,p_tmp->donnee);
		p_tmp = p_tmp->precedent;
	}
	
	return p_programme;
}
			
int analyse_syntaxique (Pile *p_pile) {
	int etat = -1;
	char sommet_pile[TAILLE_MOT],sommet_programme[TAILLE_MOT];
	int num_tab;
	
	Pile *p_programme = init();
	p_programme = empiler(p_programme, "<programme>");
	
	do { // Tant que !fin p_pile
		strcpy(sommet_pile, p_pile->donnee);
		strcpy(sommet_programme, p_programme->donnee);
		
		if (!est_motcle(sommet_pile) && strcmp(sommet_pile, "id") != 0 && strcmp(sommet_pile, "nombre") != 0) {
			if (est_constante(sommet_pile)) {
				p_pile = depiler(p_pile);
				p_pile = empiler(p_pile,"nombre");
			}

			if (est_identificateur(sommet_pile)) {
				p_pile = depiler(p_pile);
				p_pile = empiler(p_pile,"id");
			}
		}
		
		strcpy(sommet_pile, p_pile->donnee);
		strcpy(sommet_programme, p_programme->donnee);
		/* -- DEBUG
		printf("Pile programme : \n");
		affiche_pile(p_programme);
		
		printf("Pile : \n");
		affiche_pile(p_pile);
		
		printf("\n");
		printf("Sommet pile :%s\n", sommet_pile);
		printf("Sommet programme :%s\n", sommet_programme);
		printf("\n");*/
				
		if (num_tab_terminal(sommet_programme) == -1) { // Non terminal
			//printf("---- Sommet programme non terminal\n");
			strcpy(sommet_programme,p_programme->donnee);
			//printf("TA[%i][%i]\n", num_tab_nterminal(sommet_programme), num_tab_terminal(sommet_pile));
			num_tab = table_analyse(num_tab_nterminal(sommet_programme), num_tab_terminal(sommet_pile));
			p_programme = depiler(p_programme);
			//printf("num TA : %i\n", num_tab);
			
			if (num_tab != -1)
				p_programme = empiler_programme (num_tab, p_programme);
			
			else {
				printf("\nErreur dans %s\n", sommet_programme);
				etat = 0;
			}
		}
		
		else {
			//printf("---- Sommet programme terminal\n");
			
			if (strcmp(sommet_programme, sommet_pile) == 0) {
				strcpy(sommet_programme, p_programme->donnee);
				p_programme = depiler(p_programme);
				p_pile = depiler(p_pile);
				
				if (strcmp(sommet_pile,"$") == 0) {
					if (strcmp(sommet_programme,"$") == 0) 
						etat = 1; // Chaine acceptée
					else etat = 0; // Erreur
				}
			}
			
			else {
				printf("\nErreur dans %s\n", sommet_programme);
				etat = 0;
			}
		}
		
	} while (etat == -1);
	
	return etat;
}

int main (int argc[], char *argv[])
{
	Pile *p_pile = init();
	char *chaine = argv[1];
	
	printf("La chaine à analyser est : %s \n", argv[1]);
	
	printf("\n------------- Analyse lexicale ----------- \n");
	p_pile = decoupage_chaine(chaine);
	
	printf("\n----------- Analyse syntaxique ------------ \n");
	
	if (analyse_syntaxique(p_pile) == 1) {
		printf("|\n");
		printf("|__ La chaine est reconnue __\n");
	}
	else {
		printf("|\n");
		printf("|__ La chaine n'est pas reconnue __\n");
	}
	
	return 0;
}
