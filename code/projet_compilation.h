#define TAILLE_MAX 50
#define TAILLE_LETTRES 30
#define TAILLE_CHAINE 512
#define TAILLE_MOT 20

typedef struct pile
{
	char *donnee;
	struct pile *precedent;
} Pile;
